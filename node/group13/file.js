var fs = require('fs');

function write(filename, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./ssss/' + filename, content, function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done);
            }
        });
    });
}

function read(fileName, cb) {
    fs.readFile('./test/' + fileName, 'UTF-8', function (err, done) {
        if (err) {
            // console.log('error reading file <<<',err);
            cb(err);
        } else {
            // console.log('file reading success >>',done);
            cb(null, done);
        }
    })
}

function rename(oldName, newName, cb) {
    fs.rename('./test/' + oldName, './test/' + newName, function (err, done) {
        if (err) {
            cb(err)
            return;
        }
        cb(null, done);
    });
}

function remove(fileName) {
    return new Promise(function (resolve, reject) {
        fs.unlink('./test/' + fileName, function (err, done) {
            if (err) {
                reject(err);
            }
            else {
                resolve(done)
            }
        })
    })

}

// write('kisor.txt','hello');

module.exports = {
    write: write,
    read: read,
    rename: rename,
    remove: remove
}


console.log('eat food')