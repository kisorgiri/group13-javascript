// //create server
// HTTP is stateless protocol (1 request 1 response)

// // file file communication
// // import syntax es5
var http = require('http');
var fileOp = require('./file');
//we should not provide path when importing nodejs inbuilt module and modules(packages) installed in node_modules

var server = http.createServer(function (request, response) {
    // request or 1st argumet is HTTP request object
    // response or 2nd argument is http response object
    console.log('client connected to server');
    console.log('request.method >>', request.method);
    console.log('request.url >>', request.url);
    // response.end('hello from node server');
    var url = request.url;
    if (url == '/write') {
        // response.end('hello from server');
        fileOp.write('hello.txt', 'new content')
            .then(function (data) {
                response.end('file writing success');
            })
            .catch(function (err) {
                response.end('file writing failed' + err);
            })
    }
    else if (url == '/read') {
        fileOp.read('kisor.txt', function (err, done) {
            if (err) {
                console.log('err is >>', err);
                response.end('sldkjf' + err)
            } else {
                console.log('done >>>', done);
                response.end('done is ' + done);
            }
        })
    }
    else if (url == '/rename') {
        fileOp.rename('abcd.txt', 'kisor.txt', function (err, done) {
            if (err) {
                response.end('err' + err);
                return;
            }
            response.end('done' + done);
        })
    }
    else if (url === '/remove') {
        fileOp.remove('kisor.txt')
            .then(function (data) {
                response.end('data ' + data);
            })
            .catch(function (err) {
                response.end('err' + err);
            });
    }
    else {
        response.end('you are welcome');
    }


});
server.listen(4000, function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('serve listening at port 4000');
        console.log('press CTRL +C to exit');
    }
});


// when using http protocol to communicate between two programme http verb(method) are used
// get
// put
// post
// delete
// patch options

//task
// prepare 5  different file
// 4 file for different operation

// 1 prepare main file must be server
// perform appropriate action accroding to url
// try to take arguments from url

