// // promise  is object that holds future value

function buyMobile(model) {
    var mobile = new Promise(function(resolve, reject) {
        var model_exist = false;
        setTimeout(function() {
            if (model_exist) {
                resolve(model);
            } else {
                reject('model not availabel');
            }
        }, 1000);
    });
    return mobile;
}

var res = buyMobile('LG');
console.log('res >>>', res);
res
    .then(function(data) {
        console.log('i have mobile');
        console.log('now take picture');
    })
    .catch(function(err) {
        console.log('i am at error', err);
    })
    .finally(function(data) {
        console.log('promise settled >>', data);
    })

console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');
console.log('non blocking task');