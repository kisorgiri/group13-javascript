function askForNote(topic, cb) {
    console.log('friend received call for note')
    console.log('friend told to callback after finding note');

    setTimeout(function() {
        console.log('note found after hour');
        console.log('now callback with  note');
        cb(null, {
            pdfNote: topic
        })
    }, 1000);
}

function printNote(pdf, cb) {
    console.log('note is at stationary for printing');
    setTimeout(function() {
        cb(null, 'printed');
    }, 2000);
}

function watchMovie(cb) {
    console.log('start watcing movie');
    setTimeout(function() {
        cb();
    }, 2000);
}

function sleep(cb) {
    setTimeout(function() {
        cb();
    }, 200);
}

module.exports = {
    a: askForNote,
    b: printNote,
    c: watchMovie,
    d: sleep
}