
// callback is a function which is passed as an argument to another function
// callback is a function which is used to handle asynchronous result


// story to buy car
// step 1 prepare money
// step 2 visit show room
// my desired color is not available right now
// showroom owner insist me to wait for 1 week
// step 3 buy car 
// step 4 go for ride

// basic example of function
// welcome higher order function
// function welcome(name) { /// name is callback function

//     console.log('what comes in ',name);
// // a(); //call kahile pani hudaina
// }
// var a = function(){
//     console.log('i am function');
// };


// welcome(a); // a is callback function
// task part
function goToShowRoom(cb) {
    console.log('i am at show room');
    console.log('shop keeper insit me to wait for week');
    setTimeout(function(){
        console.log('car arrived at showroom after 3 hours');
        cb('jeep');
        return 'jeep(compas)'; // return will not work in time consuming task


    },3000);
}
// task part .////////////

//execution part/////
console.log('i want to buy car');
console.log('i have money');
console.log('go to showrom')
goToShowRoom(function(car){
    console.log('if this block execute');
    console.log('i have result from goToShowroom');
    console.log('i have car >>>', car);
    console.log('perform blocking task');
    console.log('now go for ride');

});
console.log('non blocking task');
console.log('eat food');
console.log('take bath');

//execution part

///summarize

///callback
//  aync execution with time consuming task

// two part of code
// exectution part where call back is used

//  task part which must be time consumeing 
// task will be function that will accept argument and argument must be function

// task 2

// prepare a story to buy cycle

// go to shop
// shopekeeper told to wait for a day
// non blocking task
// return home and eat food, go to college, wash clothes, watch tv
// after you receive cycle go to ride take photo post in instagram


