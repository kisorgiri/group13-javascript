// task part//
var task = require('./mytask');
// console.log('task >>',task);

// / execution part

console.log('start preparation for exam');
console.log('call for note');
task.a('js', function(err, done) {
    if (err) {
        console.log('err receiving note', err);
    } else {
        console.log('i received cb wih note ', done)
        console.log('note is in pdf');
        console.log('now print note for reading');
        task.b('pdfNote', function(err, done) {
            if (err) {
                console.log('printing failed', err);
            } else {
                console.log('printing success', done);
                task.c(function() {
                    console.log('movie watcing finished');
                    task.d(function() {
                        console.log('i am fresh');
                        console.log('now start preparation');
                    })
                });
                console.log('mum arrives with food');
                console.log('i receive phone call');
            }
        });
        console.log('meet friend');
        console.log('have coffee');
    }
});
console.log('eat food');
console.log('clean room');