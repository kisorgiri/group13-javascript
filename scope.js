// scope is accessibility visibility of variables(constant) function

// there are 3 types of scope
// 1 global scope 
// 2 local scope 
// 3 block scope // let vs var 
// let is es6 way of declaring variables
// var is es5 approach

let hello = "hello"; // global scope
// welcome is also global scope
// let name = 'ram';

function welcome(name) {
    // all the memory allocation within function block is local scope
    let electricity = false;
    let wel = 'welcome';
    // let a = 'hi i am a';
    // if (electricity) {
    //     let a = 'hi am inside electricity block';


    // } else {

    //     let a = 'hi i am in no electricity zone';
    //     console.log('a is >>', a);
    // }
    function test(place) {
        return hello + ' ' + name + ', ' + wel + 'to ' + place
    }
    var innerRes = test('tinkune');
    console.log('inner res >>', innerRes);

    // console.log('test >>', test);
    // console.log('hi  inside >>', hi);
    var a = test;
    return a;

}
// function sayHello(){
// 	var hi = 'i am hi';
// 	console.log('hi in sayHello ?>',hi);
// }
// sayHello();
var res = welcome('shyam');
// test();
console.log('res is  >>>', res); //hello
var resultOutside = res('hello');
console.log('resultOutside', resultOutside);


// closure
// closure  is inner function  which has access
// 1 global scope
// 2 parent function scope
// 3 parent function arugment
//  4 own scope
// own argument