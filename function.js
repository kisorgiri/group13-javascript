// primitive data 
// Number, Boolean,String,Null,Undefined

// non- primitive data type
// array object
// 1. JAVASCRIPT is Loosely typed programming language

var name;
name = 'sdlkjfd';
name = 333;
name = true;
// console.log('name >>', name);

// function is resusable block of code that performs specific task
// syntax
// function (){

// }
// types of writing function
// 1.expression syntax
// 2 declrative syntax
// 
// ###expression syntax#####
// testing()

var testing = function() {
    console.log('hello i am function name');
}

// console.log('name below >>', name());
// declarative syntax of writing function
welcome();

function welcome() {
    console.log('welcome to JAVASCRIPT');
}


// Hoisting // 
// Hoisting is mechanism that moves all the declration at top before execution


// types of function
// unnamed function (ananymous function)
// named function
// function with argument (multiple)
// function with return type
// function with argument and return type
// IIFE (immediately invoked functional expression)

// 1 named function
// function sayHello(){
// 	console.log('i am called');
// }
// sayHello();
// unnamed function ananymous function

// function (){

// }
// IIFE
// (function() {
// 	console.log('i a ananymous function which is now IIFE')
//     })()
// (sayHello)();

// function with argument
// function sayHello(name, location) {
//     // console.log('what comes in name >>>>', name);
//     // console.log('what comes in  location>>>>', location);
//     // JAVASCRIPT IS LOOSELY TYPED PL
//     console.log('hello ' + name + ' welcome to ' + location);
// }

// sayHello(null,'tinkune');
// sayHello('shyam');
// sayHello('bivek', 'butwal');

// function with return type

function sayHello(name, location) {
    // console.log('what comes in name >>>>', name);
    // console.log('what comes in  location>>>>', location);
    // JAVASCRIPT IS LOOSELY TYPED PL
    console.log('hello ' + name + ' welcome to ' + location);
    var fruits = 'apple';
    var vegitables = 'tomato';
    // return [fruits,vegitables]; //
    var abc=      {
        color: 'red',
        type: 'wall'

    }
    return abc.color;
    // return vegitables;
    console.llg('i am below');
    // javascript is loosely typed programming language
}

var res = sayHello();
console.log('res >>>', res);