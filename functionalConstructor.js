// functional constructor
// functional constructor are basic building block of prototype base oop
// how to identify functional constructor
// 1 it will never returns;
// 2 it is called using new keyword
// genrally pascalcase is used to write functional construcotr

// eg.
console.log('this global >>>', this);

function Students() { // functional constructor
    // this.name = 'ram';
    // this.address = 'tinkune';
    console.log('this inside functional constructor', this);
    // this is object of functional constructor
    // adding any property or method to this is adding values to constructor

}
// prototype keyword
// prototype property is used in functional constructor to add properties and methods in functional constructor

Students.prototype.name = 'ramesh';
Students.prototype.roll_no = '3333';
Students.prototype.getName = function(a) {
    return a;
}
var ram = new Students(); // instance
var shyam = new Students();
console.log('ram >>', ram.);
console.log('ram >>', shyam.getName('sdlkfjasdf'));

//
var str = new String('sdfkljasdlfjksdklf');

var upperString = str.toUpperCase();
console.log('upperString >>',upperString);
console.log('length >>',str.length);

var hobbies = new Array();
// Array.prototype.
hobbies.