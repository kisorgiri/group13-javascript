// // STRING
var test = '    I Am Broadway Infosys Nepal    ';

// // inbuilt methods
// // length
// console.log('length >>>', test.length);
// console.log('uppercase >>', test.toUpperCase());
// console.log('lowercase >>', test.toLowerCase());
// console.log('remove space >>>', test.trim());

// // string to array
// var hobbies = 'singing$dancing$playing$cycling';
// var arr = hobbies.split('$');
// console.log('string to array >>', arr);

// Number
// var num = '12.23';
// var num1 = 12.23;
// // console.log(num === num1);
// console.log('convert to number >', Number(num));
// // console.log('check number >', isNaN(num));
// console.log('int only >>', parseInt(num));
// console.log('fixed floating point num',num.toFixed(2));

// Boolean
// truthy and falsy properties
// falsy values => null undefined,NaN,'', 0,false,

// Object
// object collection of key value pairs
// eg 
ram = {
    name: 'ram',
    address: {
        temp_addr: ['bkt', 'chitwan', 'ktm', {
            district: 'bkt',
            wardno: 'd',
            houseNo: '44'
        }],
        permanetAddress: {
            district: 'bkt',
            wardno: 'd',
            houseNo: '44'
        }
    },
    phoneNumber: [3333, 444, 555]
}

// convert into arrays
// var keysOnly = Object.keys(ram);
// console.log('keys only >>',keysOnly);
// var valuesOnly = Object.values(ram);
// console.log('values only >>',valuesOnly);

// object as an string
// console.log('before stringify', ram);
// var str = JSON.stringify(ram);
// console.log('str >>', str);

// // revert  back to object
// var objData = JSON.parse(str);
// console.log('parsed data >>',objData);

// console.log('property exists >>', ram.hasOwnProperty('namess'));

// function sendMail(mailBody) {
//     if ('namesss' in mailBody) {
//         console.log('exists in object');
//     } else {
//         console.log('doesnot exist');
//     }
// }
// sendMail(ram);

// loop// repetation of action until certain condition is matched
// for in loop 
 
// for (var key in ram) {
// 	console.log('key >>>',key);
// 	console.log('value >>',ram[key]);
// }
// i would recommend to use for in loop for object only