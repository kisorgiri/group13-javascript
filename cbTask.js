// task part//
function askForNote(topic, cb) {
    console.log('friend received call for note')
    console.log('friend told to callback after finding note');

    setTimeout(function() {
        console.log('note found after hour');
        console.log('now callback with  note');
        cb(null, {
            pdfNote: topic
        })
    }, 1000);
}

function printNote(pdf, cb) {
    console.log('note is at stationary for printing');
    setTimeout(function() {
        cb(null, 'printed');
    }, 2000);
}

function watchMovie(cb) {
    console.log('start watcing movie');
    setTimeout(function() {
        cb();
    }, 2000);
}

function sleep(cb) {
    setTimeout(function() {
        cb();
    }, 200);
}


// / execution part

console.log('start preparation for exam');
console.log('call for note');
askForNote('js', function(err, done) {
    if (err) {
        console.log('err receiving note', err);
    } else {
        console.log('i received cb wih note ', done)
        console.log('note is in pdf');
        console.log('now print note for reading');
        printNote('pdfNote', function(err, done) {
            if (err) {
                console.log('printing failed', err);
            } else {
                console.log('printing success', done);
                watchMovie(function() {
                    console.log('movie watcing finished');
                    sleep(function() {
                        console.log('i am fresh');
                        console.log('now start preparation');
                    })
                });
                console.log('mum arrives with food');
                console.log('i receive phone call');
            }
        });
        console.log('meet friend');
        console.log('have coffee');
    }
});
console.log('eat food');
console.log('clean room');